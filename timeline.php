<?php
include_once("config/conexao.php");
$id_ocorrencia = isset($_POST['id_ocorrencia']) ? $_POST['id_ocorrencia'] : ''; //
$id = isset($_POST['id']) ? $_POST['id'] : ''; //
$sqltime="select s.descricao as descricao,s.detalhe as detalhe,substring(data_lan::text from 1 for 19) as datahora,equip,c.nome as nome  from situacao s inner join lancamento l on (s.id=l.situacao) inner join ocorrencia o on (o.id=l.id_ocorrencia) inner join clientes c on (o.idcliente=c.id) where id_ocorrencia=$id_ocorrencia order by l.id desc" ;
$res=pg_query($conexao,$sqltime);
$htmlselect="";
$validacao="1";

while ($row=pg_fetch_assoc($res)){
   $descricao = $row['descricao'];
   $detalhe = $row['detalhe'];
   $datahora = $row['datahora'];
   $equip = $row['equip'];
   $nome = $row['nome'];
   if ($validacao == ''){
      $validacao="1";
      $htmlselect=$htmlselect."  
      <div class=\"timeline-item\">
      <div class=\"timeline-icon\">
      <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
      width=\"21px\" height=\"20px\" viewBox=\"0 0 21 20\" enable-background=\"new 0 0 21 20\" xml:space=\"preserve\">
      <path fill=\"#FFFFFF\" d=\"M19.998,6.766l-5.759-0.544c-0.362-0.032-0.676-0.264-0.822-0.61l-2.064-4.999
      c-0.329-0.825-1.5-0.825-1.83,0L7.476,5.611c-0.132,0.346-0.462,0.578-0.824,0.61L0.894,6.766C0.035,6.848-0.312,7.921,0.333,8.499
      l4.338,3.811c0.279,0.246,0.395,0.609,0.314,0.975l-1.304,5.345c-0.199,0.842,0.708,1.534,1.468,1.089l4.801-2.822
      c0.313-0.181,0.695-0.181,1.006,0l4.803,2.822c0.759,0.445,1.666-0.23,1.468-1.089l-1.288-5.345
      c-0.081-0.365,0.035-0.729,0.313-0.975l4.34-3.811C21.219,7.921,20.855,6.848,19.998,6.766z\"/>
      </svg>
      </div>
      <div class=\"timeline-content right\">
      <h2>$descricao</h2>
      <p>
      $detalhe  
      </p>
      <b>
      <p>
      $datahora
      </p>
      </b>
      </div>
      </div>
      ";
   }else{
      $validacao="";
      $htmlselect=$htmlselect."
      <div class=\"timeline-item\">
      <div class=\"timeline-icon\">
      <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
      width=\"21px\" height=\"20px\" viewBox=\"0 0 21 20\" enable-background=\"new 0 0 21 20\" xml:space=\"preserve\">
      <path fill=\"#FFFFFF\" d=\"M19.998,6.766l-5.759-0.544c-0.362-0.032-0.676-0.264-0.822-0.61l-2.064-4.999
      c-0.329-0.825-1.5-0.825-1.83,0L7.476,5.611c-0.132,0.346-0.462,0.578-0.824,0.61L0.894,6.766C0.035,6.848-0.312,7.921,0.333,8.499
      l4.338,3.811c0.279,0.246,0.395,0.609,0.314,0.975l-1.304,5.345c-0.199,0.842,0.708,1.534,1.468,1.089l4.801-2.822
      c0.313-0.181,0.695-0.181,1.006,0l4.803,2.822c0.759,0.445,1.666-0.23,1.468-1.089l-1.288-5.345
      c-0.081-0.365,0.035-0.729,0.313-0.975l4.34-3.811C21.219,7.921,20.855,6.848,19.998,6.766z\"/>
      </svg>
      </div>
      <div class=\"timeline-content\">
      <h2>$descricao</h2>
      <p>
      $detalhe   
      </p>
      <b>
      <p>
      $datahora 
      </p>
      </b>
      </div>
      </div>";
   }
}
?>


<!DOCTYPE html>
<html >
<head>
   <meta charset="UTF-8">
   <title>Timeline - LUAN INFORMATICA</title>
   <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
   <link rel="stylesheet" href="css/style.css">
</head>
<body>
   <form  name="situacao" method="post"  enctype="multipart/form-data">
      <header>
         <div class="container">
            <h1 class="logo">
               LUAN<span>INFORMATICA</span>
            </h1>
            <section class="social">
               <a class="btn" href="http://www.google.com.br">Facebook</a>
               <a class="btn" href="http://www.google.com.br">Twitter</a>
            </section>
         </div>
      </header>
      <br>
      <label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Nome:<b> <?php echo ($nome); ?> </b></label> <br>
      <label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Equipamento:<b> <?php echo ($equip); ?> </b></label>
      <div class="container">
         <h1 class="project-name">Status do Equipamento</h1>
         <div id="timeline">

            <?php

            print($htmlselect);

            ?>
         </div>
      </div>
      <script src="js/index.js"></script>
   </form>
</body>
</html>