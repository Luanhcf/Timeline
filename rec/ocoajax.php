<?php
require_once '../config/conexao.php';
//Variaveis da pagina
$id        = isset($_POST['id']) ? $_POST['id'] : ''; //
$datadia   = isset($_POST['datadia']) ? $_POST['datadia'] : ''; //
$idcliente = isset($_POST['idcliente']) ? $_POST['idcliente'] : '';
$idtec     = isset($_POST['idtec']) ? $_POST['idtec'] : '';
$obs       = isset($_POST['obs']) ? $_POST['obs'] : '';
$valor     = isset($_POST['valor']) ? $_POST['valor'] : '';
$equip     = isset($_POST['equip']) ? $_POST['equip'] : '';

// Teste para inserir
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : '';

$sqlconfig = "select * from config";
$config    = pg_query($conexao, $sqlconfig);
$situacao  = isset($_GET['situacao']) ? $_GET['situacao'] : '';

// OPERAÇÃO   NOVA
if ($operacao == "novo") {
	// Capturando o id da ocorrencia para preparar o insert de lançamento
    $sql3   = "select nextval('ocorrencia_id_seq') as valor";
    $res    = pg_query($conexao, $sql3);
    $row    = pg_fetch_assoc($res);
    $idnovo = $row['valor'];
    // Consultando no banco qual a situação padrão no config
    $sql4   = "select * from config";
    $res1   = pg_query($conexao, $sql4);
    $row1   = pg_fetch_assoc($res1);
    $config = $row1['situacao'];
    // Inserindo a ocorrencia
    if(trim($valor) == ''){
        $valor=0;
    }

    $sql  = "insert into ocorrencia (id,datadia,idcliente,obs,valor,idtec,equip) values ($idnovo,'$datadia','$idcliente',upper('$obs'),$valor,$idtec,upper('$equip'))";
    $res  = pg_exec($conexao, $sql);
    // Inserindo o detalhamento da ocorrencia de acordo com o config
    $sql2 = "insert into lancamento (id_ocorrencia,situacao,data_lan) values ($idnovo,$config,current_timestamp)";
    //print("$sql2");
    $res  = pg_exec($conexao, $sql2);
} else if ($operacao == "editar"){
    $sql = "update ocorrencia set idcliente='$idcliente',obs=upper('$obs'),valor=$valor,idtec=$idtec,equip=upper('$equip') where id=$id";
    //print("$sql");
    $res = pg_exec($conexao, $sql);
    
}

header("Location: ../grid_ocorrencia.php");

?>