<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
?>
<?php
include_once("config/conexao.php");
$sqlgrid="select o.id,c.nome,datadia,o.obs,t.nome as tecnico,valor from clientes c inner join ocorrencia o on (c.id=o.idcliente) inner join tecnicos t on (o.idtec=t.id) order by id desc";
$res=pg_query($conexao,$sqlgrid);
$htmlselect3="";

?>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style3.css">
  <script type="text/javascript" src="funcoes/jquery.js"></script>
  <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
  <script type="text/javascript" src="func/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="funcoes/func_prin.js"></script> 
  <script>
    $(document).ready(function(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
  </script>
</head>
<body>
  <div id="wrapper" class="active">
   <!-- Sidebar -->
   <!-- Sidebar -->
   <div id="sidebar-wrapper">
    <ul id="sidebar_menu" class="sidebar-nav">
     <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
   </ul>
   <ul class="sidebar-nav" id="sidebar">
     <li><a href="grid_cliente.php?operacao=ativos"style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
     <ul class="sidebar-nav" id="sidebar">
      <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      <li><a href="grid_ocorrencia.php" style="color:white;">Ocorrências<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
    </ul>
    <?php
    if ($_SESSION ["podeinserir"] == 1 ){
     print("<li>
       <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>
       <li>
       <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>
       <li>
       <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>
       <li>
       <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>");
   } 
   ?>
 </ul>
 <ul class="sidebar-nav" id="sidebar">
   <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
 </ul>              
</div>
<div class="form-group col-md-5">
  <br>
  <h3>Movimentação de Ocorrências</h3>
  <br>
  <div class="col-xs-4 col-md-10 form-group">
    <input class="form-control" id="myInput" type="text" placeholder="Buscar: ID, Cliente, Data, Tecnico, OBS...">
  </div>
</div>
<br>
<table class="table">
  <tr>
   <th>Ação</th>
   <th>id</th>
   <th>Cliente</th>
   <th>Data Abertura</th>
   <th>Técnico</th>
   <th>Observação</th>
   <?php
   if ($_SESSION ["podeinserir"] == 1 ){
     print("<th>Valor</th>");
   }
   ?>
 </tr>
 <tbody id="myTable">
   <?php
   while ($row=pg_fetch_assoc($res)){
    $htmlselect3="<tr>".
    "<td><a href=\"ocorrencia.php?operacao=editar&id=".$row["id"]."\"><img height=\"14
    px\" src=\"ico/edit.png\"></a>     <a href=\"movoco.php?operacao=mov&id=".$row["id"]."\"><img height=\"16px\" src=\"ico/mais.png\"></a></td>".
    "<td>".$row["id"]."</td>".
    "<td>".$row["nome"]."</td>".
    "<td>".$row["datadia"]."</td>".
    "<td>".$row["tecnico"]."</td>".
    "<td>".$row["obs"]."</td>".
    ($_SESSION ["podeinserir"] == 1 ? "<td>".$row["valor"]."</td>":'')
    ."</tr>";
    print("$htmlselect3");
  }
  ?>
</tbody> 
</table>
<p align="center">
  <a href="ocorrencia.php?operacao=novo"><button type="button" class="btn btn-primary">Novo</button></a>
  <!--  <button type="button" class="btn btn-default">Alterar</button> -->
</p>
</div>
</div>
</form>
</div> 
</div> 
</div> 
</div>
</div>
</div>
</div> -->
</div>
</body>
</html>