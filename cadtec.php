<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("config/conexao.php");
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
  $id='';
  $nome = '';
  $endereco = '';
  $telefone ='';
  $obs ='';
}else{
$id = isset($_GET['id']) ? $_GET['id'] : ''; //
//Consulta no banco de dados edição.
$sql1= "select * from tecnicos where id = $id";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);

//Variaveis retornando do banco de dados
$nome = trim($row['nome']); //
$endereco = trim($row['endereco']);
$telefone = trim($row['telefone']);
$obs = trim($row['obs']);
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style3.css">
  <script type="text/javascript" src="funcoes/jquery.js"></script>
  <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
  <script type="text/javascript" src="funcoes/func_prin.js"></script> 
  <script language="javascript" type="text/javascript" src="func/func_cadcli.js"> </script>
</head>
<body>
  <form  name="cadcli" method="post" action="rec/tecajax.php" enctype="multipart/form-data">
    <div id="wrapper" class="active">
     <!-- Sidebar -->
     <!-- Sidebar -->
     <div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
       <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
     </ul>
     <ul class="sidebar-nav" id="sidebar">
       <li><a href="grid_cliente.php?operacao=ativos" style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
       <ul class="sidebar-nav" id="sidebar">
        <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
        <li><a href="grid_ocorrencia.php"style="color:white;">Ocorrencias<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      </ul>
      <?php
      if ($_SESSION ["podeinserir"] == 1 ){
       print("<li>
         <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>");
     } 
     ?>
   </ul>
   <ul class="sidebar-nav" id="sidebar">
     <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
   </ul>            
 </div>
 <div id="page-content-wrapper">
  <div class="page-content inset">
   <div class="row">
    <div class="col-md-12">
     <!--<p class="well lead">Cadastro de Cliente</p> -->
     <p> </p>
     <div class="container">
      <div class="row">
       <div class="col-sm-8 contact-form">
         <h3>Cadastrar Técnico</h3>
         <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
         <br>
         <div class="row">
          <div class="col-xs-6 col-md-3 form-group">
            <label>Codigo</label>
            <input class="form-control" readonly="true" id="id" value="<?php echo $id; ?>"  name="id" placeholder="id" type="text" />
          </div>
          <div class="col-xs-4 col-md-12 form-group">
            <label>Nome <b>*</b></label>
            <input class="form-control" id="nome" name="nome" value="<?php echo $nome; ?>" type="text" required />
          </div>
          
        </div>
        <div class="col-xs-4 col-md-12 form-group">
          <label>Endereço</label>
          <div class="controls">
           <input class="form-control" id="endereco" value="<?php echo $endereco; ?>" name="endereco"  type="text">
         </div>
       </div>
       <div class="col-xs-6 col-md-3 form-group">
        <label>Celular</label>
        <input class="form-control" id="telefone" value="<?php echo $telefone; ?>" name="telefone" placeholder="(99)99999-9999" maxlength="14" onKeyPress="formata_mascara(this,'(##)#####-####','#')" type="text" />
      </div>
      <div class="col-xs-4 col-md-12 form-group">
        <label>Observação</label>
        <div class="controls">
         <textarea class="form-control" id="obs" name="obs"  placeholder="Observação" rows="5"><?php echo $obs; ?></textarea>
       </div>
     </div>
     <br />
     <div class="row">
      <div class="col-xs-12 col-md-12 form-group">
       <button class="btn btn-primary pull-right" type="submit">Salvar</button>
       <button class="btn btn-primary pull-right" type="reset">Limpar</button>
       <?php
                                      //echo  $opcao;
       ?>
     </div>
   </div>
 </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</body>
</html>