<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
?>
<?php
include_once("config/conexao.php");
$datai = isset($_POST['datai']) ? $_POST['datai'] : '';
$dataf = isset($_POST['dataf']) ? $_POST['dataf'] : '';
$situacao = isset($_POST['situacao']) ? $_POST['situacao'] : '';
//$cliente = isset($_POST['cliente']) ? $_POST['cliente'] : '';

if ($datai==''){
  $datai=date("Y-m-d");
}if ($dataf==''){
  $dataf=date("Y-m-d");
}if($situacao==''){
  $situacao=0;
//}if($cliente==''){
//  $cliente=1;
}

$sql="select o.id AS id_ocorrencia, c.nome AS cliente,t.nome AS tecnico,o.equip as equip,o.obs as obs,substring(l.data_lan::text from 1 for 10) as dataa,o.valor as valor FROM   ocorrencia o inner join lancamento l ON ( o.id = l.id_ocorrencia ) inner join clientes c  ON ( o.idcliente = c.id ) inner join tecnicos t ON ( o.idtec = t.id ) WHERE  l.situacao = $situacao AND l.data_lan::date BETWEEN '$datai' AND '$dataf'";

$res=pg_query($conexao,$sql);
$htmselect="";
// SQL CONSULTA SITUACOES
$sqlsit = "select * from situacao order by descricao";
$ressit = pg_query($conexao,$sqlsit); 
$htmlselect1= "";
//$htmlselect1=$htmlselect1.("<option value=\"%\">Todos os Status</option>");
while ($row=pg_fetch_assoc($ressit)){

  $htmlselect1=$htmlselect1.("<option value=\"".trim($row["id"])."\">".$row["descricao"]."</option>");
}
//SQL PARA RETORNAR O TOTAL DE REGISTROS
$sqltotal ="select count(o.id) as total FROM   ocorrencia o inner join lancamento l ON ( o.id = l.id_ocorrencia ) inner join clientes c  ON ( o.idcliente = c.id ) inner join tecnicos t ON ( o.idtec = t.id ) WHERE  l.situacao = $situacao AND l.data_lan::date BETWEEN '$datai' AND '$dataf'";
$total=pg_query($conexao,$sqltotal);
$resultotal=pg_fetch_assoc($total);
$rtotal = $resultotal["total"];
//SQP PARA RETORNAR O TOTAL EM DINHEIRO
$sqlvlr ="select sum(o.valor) as valor FROM   ocorrencia o inner join lancamento l ON ( o.id = l.id_ocorrencia ) inner join clientes c  ON ( o.idcliente = c.id ) inner join tecnicos t ON ( o.idtec = t.id ) WHERE  l.situacao = $situacao AND l.data_lan::date BETWEEN '$datai' AND '$dataf'";
$vlr=pg_query($conexao,$sqlvlr);
$valtotal=pg_fetch_assoc($vlr);
$vtotal = $valtotal["valor"];

//$sqlcli = "select nome,id from clientes order by nome";
//$rescli = pg_query($conexao,$sqlcli); 
//$htmlcli= "";
//$htmlcli=$htmlcli.("<option value=\"%\">Todos os Clientes</option>");
//while ($row=pg_fetch_assoc($rescli)){

//  $htmlcli=$htmlcli.("<option value=\"".trim($row["nome"])."\">".$row["nome"]."</option>");
//}

?>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style3.css">
  <script type="text/javascript" src="funcoes/jquery.js"></script>
  <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
  <script type="text/javascript" src="funcoes/func_prin.js"></script> 
</head>
<body>
  <form method="POST">
    <div id="wrapper" class="active">
     <div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
       <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
     </ul>
     <ul class="sidebar-nav" id="sidebar">
       <li><a href="grid_cliente.php?operacao=ativos" style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
       <ul class="sidebar-nav" id="sidebar">
        <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
        <li><a href="grid_ocorrencia.php" style="color:white;">Ocorrências<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      </ul>
      <?php
      if ($_SESSION ["podeinserir"] == 1 ){
       print("<li>
         <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>");
     } 
     ?>
   </ul>
   <ul class="sidebar-nav" id="sidebar">
     <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
   </ul>                
 </div>
 <div class="form-group col-md-5">
  <br>
  <h3>Relatório</h3>
  <br>
  <label>Data inicial </label>
  <input class="form-control form-control-lg-2 col-md-7" name ="datai" id="datai" size="30" type="date" value="<?php echo $datai; ?>">
  <label>Data Final </label>
  <input class="form-control form-control-lg-2 col-md-7" name ="dataf" id="dataf" type="date"  value="<?php echo $dataf; ?>">
  <label>Situação</label><br>
  <select name="situacao" >
   <?php
   print("$htmlselect1");
   ?>
 </select>
 <br>
 <!--     <label>Cliente</label><br>
      <select name="cliente" >
       <?php
//         print("$htmlcli");
         ?>
       </select> -->
     </div>
     <br>
     <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">ID OCORRENCIA</th>
          <th scope="col">CLIENTE</th>
          <th scope="col">TECNICO</th>
          <th scope="col">EQUIPAMENTO</th>
          <th scope="col">OBSERVAÇÃO</th>
          <th scope="col">DATA</th>
          <th scope="col">VALOR</th>
        </tr>
        <?php
        while ($row=pg_fetch_assoc($res)){
          $htmselect="<tr>".
          "<td>".$row["id_ocorrencia"]."</td>".
          "<td>".$row["cliente"]."</td>".
          "<td>".$row["tecnico"]."</td>".
          "<td>".$row["equip"]."</td>".
          "<td>".$row["obs"]."</td>".
          "<td>".$row["dataa"]."</td>".
          "<td>".$row["valor"]."</tr>";
          print("$htmselect");
        } 
        ?>
        &nbsp&nbsp&nbsp&nbsp<label><b>Total de registros: <?php print($rtotal); ?></b></label>
        <br>
        &nbsp&nbsp&nbsp&nbsp<label><b>Valor total R$: <?php print($vtotal); ?></b></label>
        <br>
        <p align="left ">
          &nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-primary">Pesquisar</button></a>
        </p>
      </div>
    </div>
  </div>
</div> 
</div> 
</div> 
</div>
</div>
</div>
</div> 
</div>
</form>
</body>
</html>