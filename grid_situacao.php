<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
?>
<?php
include_once("config/conexao.php");
$sqlgrid="select id,descricao,detalhe,(case when encerra='true' then 'Sim' else 'Não' end) as encerra from situacao";
$res=pg_query($conexao,$sqlgrid);
$htmlselect3="";
                  //MOSTRANDO O GRID COM FUNCAO FLUSH PARA CARREGAMENTO DA PAGINA NO MOMENTO DE EXECUÇÃO DA QUERY.
?>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style3.css">
  <script type="text/javascript" src="funcoes/jquery.js"></script>
  <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
  <script type="text/javascript" src="funcoes/func_prin.js"></script> 
</head>
<body>
  <div id="wrapper" class="active">
   <!-- Sidebar -->
   <!-- Sidebar -->
   <div id="sidebar-wrapper">
    <ul id="sidebar_menu" class="sidebar-nav">
     <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
   </ul>
   <ul class="sidebar-nav" id="sidebar">
     <li><a href="grid_cliente.php?operacao=ativos" style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
     <ul class="sidebar-nav" id="sidebar">
      <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      <li><a href="grid_ocorrencia.php" style="color:white;">Ocorrências<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
    </ul>
    <?php
    if ($_SESSION ["podeinserir"] == 1 ){
     print("<li>
       <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>
       <li>
       <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>
       <li>
       <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>
       <li>
       <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
       </li>");
   } 
   ?>
 </ul>
 <ul class="sidebar-nav" id="sidebar">
   <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
 </ul>                
</div>
<div class="form-group col-md-5">
  <br>
  <h3>Situação</h3>
  <br>
  <!--    <input  type="text" class="form-control" placeholder="Busca por id ou descrição" name="busca_all"> -->
</div>
<br />
<table class="table table-hover">
  <tr>
   <th>#</th>
   <th>id</th>
   <th>Descricão</th>
   <th>Detalhamento</th>
 </tr>
 <?php
 while ($row=pg_fetch_assoc($res)){
  $htmlselect3="<tr>".
  "<td><a href=\"situacao.php?operacao=editar&id=".$row["id"]."\"><img height=\"14
  px\" src=\"ico/edit.png\"></a></td>".
  "<td>".$row["id"]."</td>".
  "<td>".$row["descricao"]."</td>".
  "<td>".$row["detalhe"]."</td>"."</tr>";

  print("$htmlselect3");
}
?>
          <!--  <tr>
               <td>1</td>
               <td>Equipamento recebido </td>
               <td>Seu equipamento já foi recebido, aguarde a analise tecnica. </td>
             </tr> -->
           </table>
           <br /><br />
           <p align="center">
            <a href="situacao.php?operacao=novo"><button type="button" class="btn btn-primary">Novo</button></a>
          </p>
        </div>
      </div>
    </form>
  </div> 
</div> 
</div> 
</div>
</div>
</div>
</div> -->
</div>
</body>
</html>