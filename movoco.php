<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
?>

<?php
include_once("config/conexao.php");
$id = isset($_GET['id']) ? $_GET['id'] : '';

$sqlgrid="select id_ocorrencia,s.descricao,data_lan::date as data_lan,substring(data_lan::text from 12 for 8) as hora,l.id From lancamento l  inner join situacao s on (l.situacao=s.id)where id_ocorrencia=$id order by l.id;";
$res=pg_query($conexao,$sqlgrid);
$htmlselect3="";

//Consulta para retornar o cliente do BD.
$sqlsit    = "select id,upper(descricao) as descricao from situacao order by id";
$sit      = pg_query($conexao, $sqlsit);
$selectsit = "";
while ($row = pg_fetch_assoc($sit)) {
  $sel = "";
    //if ($idcliente == $row['id']) {
       // $sel = "selected";
  $selectsit = $selectsit . ("<option  value=\"" . trim($row["id"]) . "\">" . $row["descricao"] . "</option>");
}

?>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style3.css">
  <script type="text/javascript" src="funcoes/jquery.js"></script>
  <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
  <script type="text/javascript" src="funcoes/func_prin.js"></script> 
</head>
<body>
  <form  name="cadcli" method="post" action="rec/lancajax.php?operacao=lancamento" enctype="multipart/form-data">
    <!--input  name="operacao" type="hidden" value='lancamento'/> -->
    <input  name="idocorrencia" type="hidden" value='<?php echo $id; ?>'/>
    <div id="wrapper" class="active">
     <!-- Sidebar -->
     <!-- Sidebar -->
     <div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
       <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
     </ul>
     <ul class="sidebar-nav" id="sidebar">
       <li><a href="grid_cliente.php?operacao=ativos"style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
       <ul class="sidebar-nav" id="sidebar">
        <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
        <li><a href="grid_ocorrencia.php" style="color:white;">Ocorrências<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      </ul>
      <?php
      if ($_SESSION ["podeinserir"] == 1 ){
       print("<li>
         <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>");
     } 
     ?>
   </ul>
   <ul class="sidebar-nav" id="sidebar">
     <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
   </ul>              
 </div>
 <div class="form-group col-md-5">
  <br>
  <h3>Lançamento de Ocorrências</h3>
  <br>
</div>

<table class="table">
  <tr>
   <th>Ação</th>
   <th>id</th>
   <th>Situação</th>
   <th>Data Movimento</th>
   <th>Hora Movimento</th>
 </tr>
 
 <?php
 while ($row=pg_fetch_assoc($res)){
  $htmlselect3="<tr>".
  "<td><a href=\"rec/lancajax.php?operacao=excluir&id=".$row["id"]."&idocorrencia=$id\"><img height=\"14
  px\" src=\"ico/excluir.png\"></a></td>".
  "<td>".$row["id_ocorrencia"]."</td>".
  "<td>".$row["descricao"]."</td>".
  "<td>".$row["data_lan"]."</td>".
  "<td>".$row["hora"]."</td>"."</tr>";
  print("$htmlselect3");
}
?>


</table>
</br></br></br></br>
<div class="row">
 <div class="col-xs-3 col-md-1 form-group">
  <input class="form-control" readonly="true" id="id" name="id" placeholder="id" type="text" required autofocus />
</div>
<select id="situacao" name="situacao" class="col-xs-3 col-md-4 form-group">
  <?php
  print("$selectsit");
  ?>
</select>
</div>
</br>
<p align="left">
 &nbsp&nbsp&nbsp <button type="submit" class="btn btn-primary">Inserir</button>
 <!--  <button type="button" class="btn btn-default">Alterar</button> -->
</p>
</div>
</div>
</div> 
</div> 
</div> 
</div>
</div>
</div>
</div> -->
</div>
</form>
</body>
</html>