<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
?>
<?php
include_once("config/conexao.php");
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
//Teste para verificar se o cadastro e novo
if ($operacao == "novo") {
  $id        = '';
  $datadia   = date("Y-m-d");
  $idcliente = '';
  $idtec     = '';
  $obs       = '';
  $valor     = '';
  $equip     = '';
} else if ($operacao == "editar") {
    $id     = isset($_GET['id']) ? $_GET['id'] : ''; //
    //Consulta no banco de dados edição.
    $sql1   = "select * from ocorrencia where id = $id";
    $ressql = pg_query($conexao, $sql1);
    $row    = pg_fetch_assoc($ressql);
    
    //Variaveis retornando do banco de dados
    $id        = trim($row['id']); //
    $datadia   = trim($row['datadia']);
    $idcliente = trim($row['idcliente']);
    $idtec     = trim($row['idtec']);
    $obs       = trim($row['obs']);
    $valor     = trim($row['valor']);
    $equip     = trim($row['equip']);
  }

//Consulta para retornar o cliente do BD.
  $sqlcli    = "select upper(nome) as nome,id from clientes where status='t' order by nome";
  $res1      = pg_query($conexao, $sqlcli);
  $selectcli = "";
  while ($row = pg_fetch_assoc($res1)) {
    $sel = "";
    if ($idcliente == $row['id']) {
      $sel = "selected";
    }
    $selectcli = $selectcli . ("<option $sel value=\"" . trim($row["id"]) . "\">" . $row["nome"] . "</option>");
  }
// Consulta para retornar tecnico do BD
  $sqltec    = "select upper(nome) as nome,id from tecnicos order by nome";
  $res2      = pg_query($conexao, $sqltec);
  $seltec = "";
  while ($row = pg_fetch_assoc($res2)) {
    $sel1 = "";
    if ($idtec == $row['id']) {
      $sel = "selected";
    }
    $seltec = $seltec . ("<option $sel value=\"" . trim($row["id"]) . "\">" . $row["nome"] . "</option>");
  }
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style3.css">
    <script type="text/javascript" src="funcoes/jquery.js"></script>
    <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
    <script type="text/javascript" src="funcoes/func_prin.js"></script> 
  </head>
  <body>
    <form  name="cadcli" method="post" action="rec/ocoajax.php" enctype="multipart/form-data">
      <div id="wrapper" class="active">
        <div id="sidebar-wrapper">
         <ul id="sidebar_menu" class="sidebar-nav">
          <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
        </ul>
        <ul class="sidebar-nav" id="sidebar">
          <li><a href="grid_cliente.php?operacao=ativos" style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
          <ul class="sidebar-nav" id="sidebar">
           <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
           <li><a href="grid_ocorrencia.php"style="color:white;">Ocorrencias<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
         </ul>
         <?php
         if ($_SESSION ["podeinserir"] == 1 ){
           print("<li>
             <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>
             <li>
             <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>
             <li>
             <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>
             <li>
             <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>");
         } 
         ?>
       </ul>
       <ul class="sidebar-nav" id="sidebar">
         <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
       </ul>
     </div>
     <div id="page-content-wrapper">
       <div class="page-content inset">
        <div class="row">
         <div class="col-md-12">
          <!--<p class="well lead">Cadastro de Cliente</p> -->
          <p></p>
          <div class="container">
           <div class="row">
            <div class="col-sm-8 contact-form">
              <h3>Lançamento de Ocorrencia</h3>
              <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
              <br>
              <div class="row">
               <div class="col-xs-6 col-md-3 form-group">
                <label>Codigo</label>
                <input class="form-control" readonly="true" value="<?php echo $id; ?>" id="id" name="id" type="text" required autofocus />
              </div>
              <?php
              if ($_SESSION ["podeinserir"] == 1 ){
                print("<div class=\"col-xs-6 col-md-3 form-group\">
                  <label>Valor R$:</label>
                  <input class=\"form-control\" id=\"valor\" name=\"valor\" value=\"$valor\" placeholder=\"Valor $\" type=\"numeric\"  />
                  </div>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                  &nbsp&nbsp&nbsp&nbsp&nbsp");
              } else if ($_SESSION ["podeinserir"] != 1 ){
               print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp");
             }
             ?>
             <div class="col-xs-6 col-md-3.5 form-group">
              <label>Data Abertura <b>*</b></label>
              <input class="form-control" id="datadia" name="datadia" value="<?php echo $datadia; ?>" placeholder="Data Abertura" type="date" required autofocus />
            </div>
          </div> 
          <div class="row">
           <div class="col-xs-4 col-md-7 form-group">
            <label>Cliente <b>*</b></label>
            <select id="idcliente" name="idcliente" class="form-control" required>
             <?php
             echo $selectcli;
             ?>
           </select>
           <!-- <input class="form-control" id="idcliente" value="<?php echo $idcliente; ?>" name="idcliente" placeholder="Cliente" type="text" /> -->
         </div>
         &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
         
         <div class="col-xs-4 col-md-4 form-group">
          <label>Tecnico <b>*</b></label>
          <select id="idtec" name="idtec" class="form-control" required="">
           <?php
           echo $seltec;
           ?>
         </select>
         <!-- <input class="form-control" id="idcliente" value="<?php echo $idcliente; ?>" name="idcliente" placeholder="Cliente" type="text" /> -->
       </div>
     </div>
     <div class="row">
      <div class="col-xs-6 col-md-12 form-group">
        <label>Equipamento/Modelo</label>
        <input class="form-control" id="equip" name="equip" value="<?php echo $equip; ?>" placeholder="Ex: DELL 2020" type="text" />
      </div>
    </div>
    <div class="row">
     <div class="col-xs-4 col-md-12 form-group">
      <label>Problema relatado</label>
      <div class="controls">
       <textarea class="form-control" id="obs" name="obs" placeholder="Ex: Formatação de Computador" rows="5"><?php echo $obs; ?></textarea>
     </div>
   </div>
   <br />
   <div class="row">
    <div class="col-xs-12 col-md-12 form-group">
     <button class="btn btn-primary pull-right" type="submit">Salvar</button>
     <button class="btn btn-primary pull-right" type="reset">Limpar</button>
   </div>
 </div>
</div> 
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</body>
</html>