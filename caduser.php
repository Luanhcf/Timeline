<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
include_once("config/conexao.php");
$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';
//Teste para verificar se o cadastro e novo
if($operacao=="novo"){
  $usuario='';
  $senha = '';
  $podeinserir = '';
}
else{
$usuario = isset($_GET['usuario']) ? $_GET['usuario'] : ''; //
//Consulta no banco de dados edição.
$sql1= "select * from usuarios where usuario = '$usuario'";
$ressql=pg_query($conexao,$sql1);
$row=pg_fetch_assoc($ressql);
//Variaveis retornando do banco de dados
//$usuario = trim($row['descricao']); //
$usuario = trim($row['usuario']);
$senha = '';
$podeinserir = trim($row['podeinserir']);
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style3.css">
  <script type="text/javascript" src="funcoes/jquery.js"></script>
  <script type="text/javascript" src="funcoes/jquery-3.3.1.min.map"></script>
  <script type="text/javascript" src="funcoes/func_prin.js"></script> 
</head>
<body>
  <form  name="situacao" method="post" action="rec/userajax.php" enctype="multipart/form-data">
    <div id="wrapper" class="active">
     <!-- Sidebar -->
     <!-- Sidebar -->
     <div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
       <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
     </ul>
     <ul class="sidebar-nav" id="sidebar">
       <li><a href="grid_cliente.php?operacao=ativos" style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
       <ul class="sidebar-nav" id="sidebar">
        <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
        <li><a href="grid_ocorrencia.php" style="color:white;">Ocorrências<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      </ul>
      <?php
      if ($_SESSION ["podeinserir"] == 1 ){
       print("<li>
         <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>
         <li>
         <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
         </li>");
     } 
     ?>
   </ul>
   <ul class="sidebar-nav" id="sidebar">
     <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
   </ul>                 
 </div>
 <div id="page-content-wrapper">
  <div class="page-content inset">
   <div class="row">
    <div class="col-md-12">
     <!--<p class="well lead">Cadastro de Cliente</p> -->
     <p> </p>
     <div class="container">
      <div class="row">
       <div class="col-sm-8 contact-form">
         <input  name="operacao" type="hidden" value='<?php echo $operacao; ?>'/>
         <h3>Cadastro de Usuario</h3>
         <br><br>
         <div class="col-xs-4 col-md-8 form-group">
          <label>Usuario</label>
          <div class="controls">
           <input class="form-control" id="usuario" value="<?php echo $usuario; ?>" name="usuario" placeholder="Usuario" required autofocus type="text">
         </div>
       </div>
       <div class="col-xs-4 col-md-8 form-group">
        <label>Senha</label>
        <div class="controls">
         <input class="form-control" id="senha" type="password" value="<?php echo $senha; ?>" name="senha" placeholder="Senha" required autofocus type="text">
       </div>
       <br> 
       <label><b>Administrador?</b></label>  
       <br>      
       <select name="podeinserir" id="podeinserir" value="" class="col-xs-6 col-md-3 form-group">
         <option <?php if($podeinserir == '1') echo 'selected'; ?> value="1">Sim</option>
         <option <?php if($podeinserir == '0') echo 'selected'; ?> value="0">Não</option>
       </select>
                                   <!-- <div class="radio">
                                       <a><b>Encerra ocorrência?</b></a> 
                                       <br>
                                       <label><input type="radio" name="sim" id="sim" value="true">Sim</label>
                                       <label>&nbsp</label>
                                       <label><input type="radio" name="nao" id="nao" value="false">Não</label>
                                     </div> -->
                                   </div>
                                   <br />
                                   <div class="row">
                                    <div class="col-xs-12 col-md-12 form-group">
                                     <button class="btn btn-primary pull-right" type="submit">Salvar</button>
                                     <button class="btn btn-primary pull-right" type="reset">Limpar</button>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </form>      
             </body>
             </html>