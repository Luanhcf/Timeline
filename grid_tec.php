<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:login.php");
  exit;
}
?>
<?php
include_once("config/conexao.php");

$operacao = isset($_GET['operacao']) ? $_GET['operacao'] : '';

if ($operacao=="issoai"){
$sqlgrid="select id,nome,telefone,obs from tecnicos order by id";
$res=pg_query($conexao,$sqlgrid);
$htmlselect3="";
}else{
$sqlgrid="select id,nome,telefone,obs from tecnicos order by id";
$res=pg_query($conexao,$sqlgrid);
$htmlselect3="";
}
                  //MOSTRANDO O GRID COM FUNCAO FLUSH PARA CARREGAMENTO DA PAGINA NO MOMENTO DE EXECUÇÃO DA QUERY.
?>
<html>
   <head>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/style3.css">
      <script type="text/javascript" src="func/jquery.js"></script>
      <script type="text/javascript" src="func/jquery-3.3.1.min.map"></script>
      <script type="text/javascript" src="func/func_prin.js"></script> 
      <script type="text/javascript" src="func/func_busca.js"></script>
   </head>
   <body>
      <div id="wrapper" class="active">
         <!-- Sidebar -->
         <!-- Sidebar -->
         <div id="sidebar-wrapper">
            <ul id="sidebar_menu" class="sidebar-nav">
               <li class="sidebar-brand"><a id="menu-toggle" href="home.php" style="color:white;">Home<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
            </ul>
            <ul class="sidebar-nav" id="sidebar">
               <li><a href="grid_cliente.php?operacao=ativos" style="color:white;">Clientes<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
               <ul class="sidebar-nav" id="sidebar">
                  <li><a href="grid_situacao.php" style="color:white;">Situação<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
                  <li><a href="grid_ocorrencia.php" style="color:white;">Ocorrências<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
               </ul>
             <?php
             if ($_SESSION ["podeinserir"] == 1 ){
       print("<li>
             <a href=\"grid_tec.php?operacao=issoai\" style=\"color:white;\">Técnicos<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>
             <li>
             <a href=\"relatorios.php\" style=\"color:white;\">Relatorio<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>
            <li>
             <a href=\"user.php\" style=\"color:white;\">Usuários<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>
              <li>
             <a href=\"config.php\" style=\"color:white;\">Configurações<span class=\"sub_icon glyphicon glyphicon-link\"></span></a>
             </li>");
               } 
            ?>
            </ul>
           <ul class="sidebar-nav" id="sidebar">
         <li><a href="logout.php" style="color:white;">Sair<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
      </ul>
         </div>
         <div class="form-group col-md-5">
            <br>
            <h3>Técnicos</h3>
            <br>
         <!--   <input type="text" name="busca" id="myInput" onkeyup="mySearch()" class="form-control" placeholder="Busca por codigo ou nome ou telefone ou celular" name="busca_all"> -->
         </div>
         <br />
         <table class="table">
            <tr>
               <th>#</th>
               <th>id</th>
               <th>Nome</th>
               <th>Telefone</th>
               <th>OBS</th>
            </tr>
        
          <?php
              while ($row=pg_fetch_assoc($res)){
                        $htmlselect3="<tr>".
                        "<td><a href=\"cadtec.php?operacao=editar&id=".$row["id"]."\"><img height=\"14
                        px\" src=\"ico/edit.png\"></a></td>".
                        "<td>".$row["id"]."</td>".
                        "<td>".$row["nome"]."</td>".
                        "<td>".$row["telefone"]."</td>".
                        "<td>".$row["obs"]."</td>"."</tr>";
                        print("$htmlselect3");
                      }
              ?>


              
         </table>
         <br /><br />
         <p align="center">
            <a href="cadtec.php?operacao=novo"><button type="button" class="btn btn-primary">Novo</button></a>
         </p>
         <!-- Page content 
            <div id="page-content-wrapper">
            
              <div class="page-content inset">
                  <div class="row">
                    <div class="col-md-12">
                    <p class="well lead">Cadastro de Cliente</p>
                    <div class="container">
                      <div class="row"> 
                          
                          <div class="col-sm-8 contact-form"> 
                              <form id="contact" method="post" class="form" role="form">
                                  <div class="row">
                                      <div class="col-xs-6 col-md-3 form-group">
                                          <input class="form-control" id="inputCNPJ" name="CNPJ" placeholder="CNPJ" type="text" required autofocus />
                                      </div>
                                      <div class="col-xs-4 col-md-9 form-group">
                                          <input class="form-control" id="inputrazaosocial" name="razaocosial" placeholder="Razão Social" type="text" />
                                      </div>
                                  </div> 
                                  
                                  <div class="col-xs-4 col-md-12 form-group">
                                      <div class="controls">
                                          <input class="form-control" id="inputendereco" name="endereco" placeholder="Endereço"  type="text">
                                      </div>
                                  </div>
                                  <br> 
                                 
                                  <div class="row">
                                      <div class="col-xs-6 col-md-9 form-group">
                                          <input class="form-control" id="inputcidade" name="cidade" placeholder="Cidade" type="text" />
                                      </div>
                                      <div class="col-xs-4 col-md-3 form-group">
                                          <select class="form-control"id="selectbasic" name="selectestado" >
                                              <option>Selecione</option>
                                              <option>AC</option>
                                              <option>AL</option>
                                              <option>AP</option>
                                              <option>AM</option>
                                              <option>BA</option>
                                              <option>CE</option>
                                              <option>DF</option>
                                              <option>ES</option>
                                              <option>GO</option>
                                              <option>MA</option>
                                              <option>MT</option>
                                              <option>MS</option>
                                              <option>MG</option>
                                              <option>PA</option>
                                              <option>PB</option>
                                              <option>PR</option>
                                              <option>PE</option>
                                              <option>PI</option>
                                              <option>RJ</option>
                                              <option>RN</option>
                                              <option>RS</option>
                                              <option>RO</option>
                                              <option>RR</option>
                                              <option>SC</option>
                                              <option>SP</option>
                                              <option>SE</option>
                                              <option>TO</option>
                                          </select>
                                      </div>
                                      <div class="col-xs-6 col-md-3 form-group">
                                          <input class="form-control" id="inputtelefone" name="telefone" placeholder="Telefone" type="text" />
                                      </div>
                                      <div class="col-xs-4 col-md-3 form-group">
                                          <input class="form-control" id="inputcontato" name="contato" placeholder="Contato" type="text" />
                                      </div>
                                      <div class="col-xs-4 col-md-6 form-group">
                                          <input class="form-control" id="inputemail" name="email" placeholder="E-mail" type="text" />
                                      </div>
                                  </div>
                                  <div class="col-xs-4 col-md-12 form-group">
                                      <div class="controls">
                                          <textarea class="form-control" id="message" name="message" placeholder="Message" rows="5"></textarea>
                                      </div>
                                  </div>
                                  <br />
                                  
                                  <div class="row">
                                      <div class="col-xs-12 col-md-12 form-group">
                                          <button class="btn btn-primary pull-right" type="submit">Salvar</button>
                                          <button class="btn btn-primary pull-right" type="submit">Limpar</button>
                                          type="submit">Enviar</button>-->
      </div>
      </div>
      </form>
      </div> 
      </div> 
      </div> 
      </div>
      </div>
      </div>
      </div> -->
      </div>
   </body>
</html>