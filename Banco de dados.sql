
-- DROP DATABASE timeline;

CREATE DATABASE timeline
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;

  -- Table: clientes

-- DROP TABLE clientes;

CREATE TABLE clientes
(
  id serial NOT NULL,
  nome character(100),
  endereco character(100),
  cidade character(50),
  telefone character(14),
  contato character(13),
  email character(100),
  cpf character(14),
  obs character(1000),
  status boolean,
  CONSTRAINT id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE clientes
  OWNER TO postgres;
-- Table: config

-- DROP TABLE config;

CREATE TABLE config
(
  situacao numeric,
  datavalidade date,
  id serial NOT NULL,
  CONSTRAINT config_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE config
  OWNER TO postgres;

  -- Table: lancamento

-- DROP TABLE lancamento;

CREATE TABLE lancamento
(
  id serial NOT NULL,
  id_ocorrencia numeric,
  situacao numeric,
  data_lan timestamp with time zone,
  CONSTRAINT lancamento_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE lancamento
  OWNER TO postgres;

  -- Table: ocorrencia

-- DROP TABLE ocorrencia;

CREATE TABLE ocorrencia
(
  id serial NOT NULL,
  idcliente integer,
  obs character(1000),
  datadia date,
  valor numeric(13,2),
  idtec integer,
  equip character(200),
  CONSTRAINT ocorrencia_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ocorrencia
  OWNER TO postgres;

  -- Table: situacao

-- DROP TABLE situacao;

CREATE TABLE situacao
(
  id serial NOT NULL,
  descricao character(100),
  detalhe character(500),
  encerra boolean,
  CONSTRAINT situacao_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE situacao
  OWNER TO postgres;

  -- Table: tecnicos

-- DROP TABLE tecnicos;

CREATE TABLE tecnicos
(
  id serial NOT NULL,
  nome character(200),
  endereco character(200),
  telefone character(15),
  obs character(1000),
  CONSTRAINT tecnicos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tecnicos
  OWNER TO postgres;

  -- Table: usuarios

-- DROP TABLE usuarios;

CREATE TABLE usuarios
(
  usuario character varying(20) NOT NULL,
  senha character varying(32),
  podeinserir integer,
  CONSTRAINT usuarios_pkey PRIMARY KEY (usuario)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuarios
  OWNER TO postgres;

-- INSERT
insert into usuarios (usuario,senha,podeinserir) values ('adm','b09c600fddc573f117449b3723f23d64','1');
insert into config (situacao) values (1);